// ******************************** //
// ** Writen by Josiah Niedrauer ** //
// ******************************** //
var { Cc, Ci, Cu } = require('chrome');
var wm = Cc["@mozilla.org/appshell/window-mediator;1"].getService(Ci.nsIWindowMediator);
var enumerator = wm.getEnumerator("navigator:browser");

Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/BrowserUtils.jsm");

var mozElts = ["Browser:AddBookmarkAs",
                "Browser:Back",
                "Browser:BackOrBackDuplicate",
                "Browser:BookmarkAllTabs",
                "Browser:Forward",
                "Browser:Forward",
                "Browser:ForwardOrForwardDuplicate",
                "Browser:Home",
                "Browser:ReloadOrDuplicate",
                "Browser:Stop",
                "Browser:ToggleAddonBar",
                "Browser:ToggleTabView",
                "cmd_find",
                "cmd_findAgain",
                "cmd_findPrevious",
                "cmd_fullZoomEnlarge",
                "cmd_fullZoomReduce",
                "cmd_fullZoomReset",
                "cmd_fullZoomToggle",
                "cmd_handleBackspace",
                "cmd_handleShiftBackspac",
                "cmd_newNavigatorTab",
                "editMenuCommands",
                "History:UndoCloseTab",
                "History:UndoCloseWindow",
                "Tools:Addons",
                "Tools:Downloads",
                "Tools:PrivateBrowsing",
                "Tools:Sanitize"];

var xulIds =    ["#navigator-toolbox",
                "#fullscr-toggler",
                "#PersonalToolbar"];

var keyMap = {  F:      70,
                G:      71,
                P:      80,
                Q:      81,
                S:      83,
                T:      84,
                W:      87,
                F11:    122,
                Slash:  191,
                SQuote: 222 };

var domElt, doc, menu, qs, pv;

function _shouldFastFind() {
    let lastWindow = Services.wm.getMostRecentWindow(null);
    // the following 4 lines mirror Mozilla's codebase as closely as
    // possible, so that we can preventDefault on these keys only when
    // firefox would actually call quickfind
    let focusedWindow = {};
    let elt = Services.focus.getFocusedElementForWindow(lastWindow, true, focusedWindow);
    let win = focusedWindow.value;
    return BrowserUtils.shouldFastFind(elt, win);
}

// Iterate through bottom level windows
while (enumerator.hasMoreElements()) {
    let self = enumerator.getNext();
    // Firefox preserves its state when closed, so maximizing speeds up future start times
    self.maximize();
    self.BrowserFullScreen();

    self.addEventListener('keydown', function(e) {
        // Disable / and '
        if ((e.which == keyMap.Slash | e.which == keyMap.SQuote) && _shouldFastFind()) {
            e.preventDefault();
        }
        // Disable F11
        if (e.which == keyMap.F11) {
            e.preventDefault();
        }   
        // Disable Ctrl+F/G/P/Q/S/T/W
        if (e.ctrlKey && (e.which == keyMap.F | e.which == keyMap.G | e.which == keyMap.P |
           e.which == keyMap.Q | e.which == keyMap.S | e.which == keyMap.T | e.which == keyMap.W)) {
            e.preventDefault();
        }
    }
    , false);

    doc = self.window.document;

    for (let elt of mozElts) {
        domElt = doc.getElementById(elt);
        if (domElt) {
            domElt.setAttribute("disabled", "true");
        }
    }

    // Prevent right click context menus
    menu = doc.getElementById("contentAreaContextMenu");
    if (menu) {
        menu.addEventListener("popupshowing", function(e) {
            e.preventDefault();
        }, false);

    }

    // Hide chrome level DOM elements we don't want to see
    for (let xulId of xulIds) {
        qs = doc.querySelector(xulId);
        if (qs) {
            pv = qs.style.getPropertyValue("display") == "none";
            qs.style.setProperty("display",pv?xulId:"none","important");
        }
    }
}
