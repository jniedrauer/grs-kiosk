#GRS Kiosk Extension
This addon will hide the browser bar and the invisible
browser bar dropdown trigger, set the browser to
full screen, and disable the F11 toggle shortcut.

To compile:

npm install jpm

jpm xpi
